#!/bin/bash

appliance-creator -c container-small-20.ks -d -v -t /tmp     \
     -o /tmp/f20_small --name "fedora-20-small" --release 20 \
     --format=qcow2 && 
virt-tar-out -a /tmp/f20_small/ame/ame-sda.qcow2 / - | docker import - chedi/f20_mini

